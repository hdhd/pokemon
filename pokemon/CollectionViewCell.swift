//
//  CollectionViewCell.swift
//  pokemon
//
//  Created by NGUYEN HUU DANG on 8/30/16.
//  Copyright © 2016 NGUYEN HUU DANG. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var pokeImage: UIImageView!
    @IBOutlet weak var pokeName: UILabel!
    @IBOutlet weak var pokeId: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setUp(index: Int){
        
        self.pokeImage.image = UIImage(named: "\(index).png")
        self.pokeName.text = "#\(index)"
        
    }

}
