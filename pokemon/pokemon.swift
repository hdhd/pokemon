//
//  pokemon.swift
//  pokemon
//
//  Created by NGUYEN HUU DANG on 8/31/16.
//  Copyright © 2016 NGUYEN HUU DANG. All rights reserved.
//

import Foundation
import SwiftyJSON


class Pokemon {
    
    var name: String?
    
    
    init(poke: JSON){
        self.name = poke["ja"].string
    }
//    let number: Int
//    let color: Int
//    let weaknesses: [String]
    
    
    class func getPoke() -> [Pokemon]{
        
        let path = NSBundle.mainBundle().pathForResource("pokemon", ofType: "json")!
        let jsonData = NSData(contentsOfFile: path)!
//        let pokeData = SwiftyJSON.JSON(jsonData)
        let pokeData = JSON(data: jsonData)
         var pokeList = [Pokemon]()
        pokeData.forEach{ (_,poke) in
        
            pokeList.append(Pokemon(poke: poke))
        }
        
        return pokeList
        
    }
}
