//
//  ViewController.swift
//  pokemon
//
//  Created by NGUYEN HUU DANG on 8/30/16.
//  Copyright © 2016 NGUYEN HUU DANG. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    var color: UIColor = UIColor(red: 0.9098, green: 0.9725, blue: 1, alpha: 1.0)
    
    var pokemon: [Pokemon]?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = color
        self.collectionView.backgroundColor = color
        self.pokemon = Pokemon.getPoke()
        // Do any additional setup after loading the view, typically from a nib.
        collectionView.delegate = self
        collectionView.dataSource = self
    collectionView.registerNib(UINib(nibName: "CollectionViewCell", bundle: nil) , forCellWithReuseIdentifier: "CollectionViewCell")
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //データの個数を返すメソッド
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 151
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        //return CGSizeMake(120, 150)

        let margin = 10
        
        let numberOfColumns = 3
        
        let itemWidth = (self.collectionView.frame.size.width - CGFloat(margin)) / CGFloat(numberOfColumns)
        
        return CGSize(
            width: itemWidth - CGFloat(margin),
            height: 150 )
    }
    
    
    //データを返すメソッド
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        //コレクションビューから識別子「TestCell」のセルを取得する。
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionViewCell", forIndexPath: indexPath) as! CollectionViewCell
        
        //セルの背景色をランダムに設定する。
        cell.pokeImage.backgroundColor = UIColor(red: CGFloat(drand48()),
                                                 green: CGFloat(drand48()),
                                                 blue: CGFloat(drand48()),
                                                 alpha: 1.0)
        cell.backgroundColor = UIColor.whiteColor()
        
        cell.pokeId.text = self.pokemon![indexPath.row].name ?? ""
        cell.setUp(indexPath.row + 1)
        
        
        return cell
        
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {

    }
    


}

